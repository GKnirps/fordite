Fordite Examples
================

Configuration Files
-------------------

This directory contains some example configuration files.

At the point of this writing, every available configuration parameter is used in at least one configuration file. A comprehensive documentation of all parameters does not exist (yet), but from looking at the sample files one can get a pretty good idea how they work.

Out of laziness, there is no detailed description (yet) of how most of the different parameters work. Just look at the examples (or the code) and figure it out yourself). Mind that all this is a work in progress and will likely change (i.e. breaking changes may occur)

### General Structure

Each configuration file is a JSON list of single configurations. Each configuration describes one particular image.

Inside an image, there are several parameters, here is a rough list:

- `width` and `height` (unsigned integer): The width and the height of the output image (in pixel)
- `max_layer` (64 bit float): The height of the three-dimensional space (where `width` and `height` define the base rectangle). Height maps will be scaled to the interval `[1, max_layer]`. Some layer schemes create a different number of color layers depending on the maximum height.
- `output` (string): optional output file name. If not present, it will save the output in `./out.png`. IMPORTANT: fordite does not check if there are duplicates in the list of configurations or performs any other type of sanity check. Check your configuration file to make sure you don't accidentally overwrite important files!
- `height_map_orientation` (floating point number): optional radian angle on how the height map is rotated before application
- `height_map_trans_x`, `height_map_trans_y`: optional translation of the height map before it is applied (if not present, the height map is centered on `(0,0)` in the image)
- `height_map`: height map type that is applied. Some height maps have interior parameters specific to the type. See below for examples
- `palette`: an optional list of colors to pick from for the color layers. If absent, a default (and rather gaudy) palette will be used
- `layers`: enum how the color layers are created. May contain interior parameters specific to type later (but not yet)
- `rand_seed`: optional seed for the RNG. If the seed is given, every image will be generated the same way, otherwise the color layers will differ

Example Files
-------------

### Height Maps

`height_maps.json` contains a list of configurations for all different height maps. For the color layer generation, `display_height_map` is chosen, which does not result in a pretty image but is useful to see how the height map looks (black: low, white: high).

For efficiency, all of these examples are `256×256` in size. This leads to a lot of rounding errors, so don't judge their prettiness by looking at the small images. Try to render them in a larger image.

At the point of this writing, there are four available height maps:

![slope](img/hm_slope.webp)
![elliptic paraboloid](img/hm_el_parab.webp)
![hyperbolic paraboloid](img/hm_hy_parab.webp)
![sine](img/hm_sine.webp)
![two sine waves with different orientation added up](img/hm_twosine.webp)
![sine waves in circular form around a center](img/hm_polar_sine.webp)
![an entirely white image: the flat height map](img/hm_flat.webp)
![a more natural looking height map](img/hm_diamond_square.webp)

From left to right, top to bottom:
- `slope`: a rising slope, starting at the bottom at `x = 0` and continuously rising at the same rate over the image
- `elliptic_paraboloid`: an elliptic paraboloid turned upside down (highest point is at `(x, y) = (0, 0)`
- `hyperbolid_paraboloid`: a hyperbolic paraboloid (`z = x² - y²`) 
- `sine` a sine wave. This one has an interior parameter `period` which defines the period of the wave (in pixels)
- `sum` two sine waves of different orientation and possibly different amplitude and frequency added together. Any number of different height maps can be added. Internally, each height map is normalized first, then multiplied by a given weight (default: 1.0) and then added to the other height maps.
- `polar_sine` a sine wave whose phase depends on the distance of the origin
- `flat` a flat, horizontal height map where the height is `max_layer`. Meant for testing color layers, looks very boring for most use cases
- `diamond_square` generates a random fractal landscape using the [diamond-square algorithm](https://en.wikipedia.org/wiki/Diamond-square_algorithm). Don't ask me why it's called “diamond-square“ when it only handles squares (half of them rotated by 45°). The parameter `smoothness` is the exponent for the decrease of the magnitude of the random value in each step. Height map transposition and rotation are ignored for this height map.

Please note that in this example, the paraboloids are moved to the center of the image, to better demonstrate them.

### Layer types

The other example json files demonstrate different ways to generate the color layers.

#### Horizontal Planes

The simplest way for the color layers is to simply lay the colors in horizontal planes, where each plane fills the whole image. This way, the color of a pixel only depends on the height.

![horizontal planes of different colors, with a two-sine height map applied to it](img/twosine_horizontal_planes.webp)

In this case, a two-sine height map was applied, where one sine was at an about 45° angle to the other and had twice its amplitude.

#### Inclined Planes

The next options is basically the same as the horizontal planes, but they are rotated around the `y` axis, so a layer of the same color rises along the `x` axis. They also do not reach the same height everywhere (this is subject to change).

![inclined planes of different colors, with a hyperbolic paraboloid height map applied to it](img/h_parab_inclined_planes.webp)

Here, a hyperbolic paraboloid height map (not centered) has been applied to the color layers. Note the area at the left, where there is a large monochrome area: This is because the colors do not fill the entire height (as mentioned above).

#### Horizontal Half Planes

This is what is left over of an earlier experiment that did not quite work out the way I hoped. However, it can still lead to interesting images. The idea is to first paint the left half of the image, then the top half, then the right half, the bottom half and so on.

![horizontal half planes of different colors, with a rotated slope height map applied to it](img/slope_half_planes.webp)

In this case, a rotated slope height map was applied to it. Characteristic for this layer generation is the split into four different quadrants, each with a different (albeit related) layer scheme.

#### Circles

Another layer scheme that will probably receive some updates in the future. The original idea was to paint circles of different sizes all over the place, so that each circle potentially stretched over different heights, depending on where it painted over other circles.

![in the left half of the image, partial circles are visible. In the left half, stripes with circular borders are visible. The center looks like a memory corruption error](img/madness.webp "If I hadn't written this in rust, I would have assumed his was a memory corruption problem")

However, the results for this looked really weird. In an attempt to salvage it, I made the circle radius fixed size (depending in the image size) and large. This ist what I want to make configurable at some point.

![circles of different colors, with a rotated sine height map applied to it](img/sine_circles.webp)

It is the sine height map again. I really like the sine height map, because it gives the most interesting images so far. It even manages to make this circle-thing work after a fashion.

#### Circle Planes

After the first weird results with the circles, I tried a different approach: Do the same thing, but fill the entire layer: One color for the circle, another color for the rest of the plane.

![circle planes with an elliptic paraboloid height map applied to it](img/parab_circle_planes.webp)

I used the elliptic paraboloid height map and I must say I kind of like the result.

#### Spheres

After the first few attempts, I used a different approach. While I used some non-linear height maps, the first attempts all used rather linear color combinations (the circle and the horizontal half planes being notable exceptions, but they did not yield the results I was hoping for either).

So I tried to layer the colors in a sphere (or, originally, a hemisphere):

![sphere layers with a rotated sine heightmap applied to it](img/sine_sphere.webp)

I found out that combining some height maps with the sphere did not result in interesting images. The paraboloids just looked similar to the ones with the simple horizontal planes (albeit a bit shifted), the slope did not much better.

The sine height map however, produced a rather nice image (as seen above).

The sphere is also one of the layer schemes that will receive more configurability later, e.g. moving the center (horizontally, probably also vertically, thus making it not a sphere anymore).

#### Cylinder

Another  non-linear approach to color layers are cylinder-shaped color layers. Here is one with a simple hyperbolic paraboloid height map:

![cylinder layers with a centered hyperbolic paraboloid height map](img/h_parab_cylinder.webp)

In this case the cylinder goes from the origin along an `(1,1,1)` vector, the center of the paraboloid is at the center of the image.

#### Barber's Pole

From the outside, this cylindric thing looks kind of like a barber's pole, lines of different color spiraling in one direction.

In the inside, it is just slices of color that shift along the main axis of the cylinder.

For illustration: the next image here shows the barber's pole from the top with increasing height.
The second image shows a rotating barber's pole from the side, projected on a 2D surface.

![a red, white and blue quandrants of an image rotate around the image's center](img/barber_pole_top_animated.png)
![diagonal red, blue and white lines move from right to left](img/barber_pole_side_animated.png)

In this example, a polar sine height map is used. The pole goes from the top left corner on the bottom of the height map to the bottom right corner at the top of the height map.

![barber pole layers with a sine height map](img/barber_pole.webp)

#### Other examples

Here is an example how a fractal landscape (diamond-square) looks like when it is applied to sphere color layers:

![shades of green with irregular borders between them](img/diamond_square_sphere.webp)

### Animations

Even though the original idea was to have a static image (since real polished fordite also stays in the same form) I found that you can generate rather nice animations, e.g. by rotating and shifting the height map over time.

`sine_sphere_video.py` creates configuration data for a series of images based on the sphere with the sine height map (seen above). It prints the output to `stdout`, so it is possible to just pipe it to fordite.

I created a video out of the frames with `ffmpeg -vsync 0 -threads 2 -stream_loop 5 -r 25 -framerate 25 -i "frame_%04d.png" -c:v libvpx-vp9 -pix_fmt yuva420p -lossless 0 video.webm` and [uploaded it to youtube](https://www.youtube.com/shorts/CaWXGV3QlO0). I will probably add a higher resolution version of it (with a more fitting aspect ratio) later.
