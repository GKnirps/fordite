#!/usr/bin/env python3

import math
import json

# this script generates a configuration for fordite.
# Each frame will vary slightly from the previous one,
# so they can be put together as an animation

n_frames = 64

def top_frame(i):
    return {
        "width": 256,
        "height": 256,
        "max_layer": i,
        "output": "barber_pole_top_%04d.png" % i,
        "height_map": {"type": "flat"},
        "palette": ["ff0000", "ffffff", "0000c8"],
        "layers": {
            "type": "barber_pole",
            "params": {
                "origin": [128, 128, 128],
                "orientation": [0, 0, 1],
                "interval": 64,
                "num_slices": 4,
            },
        },
        "rand_seed": 1234567,
    }

def side_frame(i):
    return {
        "width": 256,
        "height": 256,
        "max_layer": 128,
        "output": "barber_pole_side_%04d.png" % i,
        "height_map": {"type": "flat"},
        "palette": ["ff0000", "ffffff", "0000c8"],
        "layers": {
            "type": "barber_pole",
            "params": {
                "origin": [0, 128, 0],
                "orientation": [1, 0, 0],
                "interval": 64,
                "num_slices": 4,
                "offset": 2 * math.pi * i*2 / n_frames,
            },
        },
        "rand_seed": 1234567,
    }


config = list(map(top_frame, range(n_frames)))
config.extend(map(side_frame, range(n_frames//2)))

print(json.dumps(config))
