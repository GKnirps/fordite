/*
 * This file is part of fordite.
 *
 *  fordite is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  fordite is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with fordite. If not, see <https://www.gnu.org/licenses/>.
 */
#![forbid(unsafe_code)]

mod gems;
mod height_maps;
mod linear_algebra;

use crossbeam::channel;
use gems::LayersConfig;
use image::Rgba;
use serde::{
    de::{self, Deserializer, Visitor},
    Deserialize,
};
use std::cmp::min;
use std::env;
use std::fmt;
use std::fs::File;
use std::io::BufReader;
use std::path::{Path, PathBuf};
use std::thread;

use height_maps::{Dimensions, HeightMapConfig};

#[derive(Clone, Debug, Deserialize)]
pub struct Config {
    #[serde(flatten)]
    dimensions: Dimensions,
    output: Option<PathBuf>,
    height_map: HeightMapConfig,
    #[serde(default)]
    palette: Vec<ColorConfig>,
    layers: LayersConfig,
    rand_seed: Option<u64>,
}

#[derive(Copy, Clone, PartialEq, Eq, Debug)]
struct ColorConfig(Rgba<u8>);
struct ColorVisitor;

impl<'de> Visitor<'de> for ColorVisitor {
    type Value = ColorConfig;

    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        formatter.write_str("a hexadecimal encoded RGB value in the form XXXXXX")
    }

    fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        if v.len() != 6 {
            return Err(de::Error::custom("not a 6-digit hex value"));
        }
        if !v.chars().all(|c| c.is_ascii_hexdigit()) {
            return Err(de::Error::custom("not a 6-digit hex value"));
        }
        let r = u8::from_str_radix(&v[0..2], 16).map_err(|e| de::Error::custom(e.to_string()))?;
        let g = u8::from_str_radix(&v[2..4], 16).map_err(|e| de::Error::custom(e.to_string()))?;
        let b = u8::from_str_radix(&v[4..6], 16).map_err(|e| de::Error::custom(e.to_string()))?;

        Ok(ColorConfig(Rgba([r, g, b, 255])))
    }
}

impl<'de> Deserialize<'de> for ColorConfig {
    fn deserialize<D>(deserializer: D) -> Result<ColorConfig, D::Error>
    where
        D: Deserializer<'de>,
    {
        deserializer.deserialize_str(ColorVisitor)
    }
}

fn read_config_files<P: AsRef<Path>>(filenames: &[P]) -> Result<Vec<Config>, String> {
    let mut configs: Vec<Config> = Vec::with_capacity(16);
    for res in filenames.iter().map(|p| read_config_file(p.as_ref())) {
        let mut cfg = res?;
        configs.append(&mut cfg);
    }
    Ok(configs)
}

fn read_config_file(filename: &Path) -> Result<Vec<Config>, String> {
    let f = File::open(filename).map_err(|e| {
        format!(
            "Unable to open file '{}': {}",
            filename.to_string_lossy(),
            e
        )
    })?;
    let mut reader = BufReader::new(f);
    serde_json::from_reader(&mut reader).map_err(|e| {
        format!(
            "Unable to read or parse config file '{}': {e}",
            filename.to_string_lossy()
        )
    })
}

fn read_stdin_config() -> Result<Vec<Config>, String> {
    let mut reader = BufReader::new(std::io::stdin());
    serde_json::from_reader(&mut reader)
        .map_err(|e| format!("Unable to parse config from stdin: {e}"))
}

fn main() -> Result<(), String> {
    let config_filenames: Vec<PathBuf> = env::args().skip(1).map(PathBuf::from).collect();
    let configs = if config_filenames.is_empty() {
        read_stdin_config()
    } else {
        read_config_files(&config_filenames)
    }?;

    let (sender, receiver) = channel::unbounded();

    let n_configs = configs.len();

    let sender_handle = thread::spawn(move || {
        for config in configs {
            let _ = sender.send(config);
        }
    });

    let max_parallel = thread::available_parallelism()
        .map(|n| n.get())
        .unwrap_or(4);
    let worker_handles: Vec<thread::JoinHandle<Result<(), String>>> =
        (0..min(max_parallel, n_configs))
            .map(|_| {
                let rec = receiver.clone();
                thread::spawn(move || {
                    for config in rec {
                        gems::create_image(&config).map_err(|e| e.to_string())?;
                    }
                    Ok(())
                })
            })
            .collect();

    let mut results: Vec<Result<(), String>> = Vec::with_capacity(1 + worker_handles.len());
    results.push(
        sender_handle
            .join()
            .map_err(|_| "producer thread panicked".to_string()),
    );

    for handle in worker_handles {
        results.push(
            handle
                .join()
                .map_err(|_| "worker thread panicked".to_string())
                .and_then(|r| r),
        );
    }

    results.into_iter().find(|r| r.is_err()).unwrap_or(Ok(()))
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn color_config_parses_from_valid_json() {
        // given
        let json = "\"11bbAA\"";

        // when
        let c = serde_json::from_str::<ColorConfig>(json);

        // then
        assert_eq!(
            c.expect("expected success"),
            ColorConfig(Rgba::<u8>([0x11, 0xbb, 0xaa, 0xff]))
        );
    }

    #[test]
    fn color_config_fails_for_invalid_json() {
        // too long
        assert!(serde_json::from_str::<ColorConfig>("\"11223344\"").is_err());
        // too short
        assert!(serde_json::from_str::<ColorConfig>("\"11223\"").is_err());
        // not hex
        assert!(serde_json::from_str::<ColorConfig>("\"11223G\"").is_err());
        // multi-byte scalar values
        assert!(serde_json::from_str::<ColorConfig>("\"1ä2ä\"").is_err());
        // empty string
        assert!(serde_json::from_str::<ColorConfig>("\"\"").is_err());
        // not a string
        assert!(serde_json::from_str::<ColorConfig>("true").is_err());
        assert!(serde_json::from_str::<ColorConfig>("42").is_err());
        assert!(serde_json::from_str::<ColorConfig>("[1,2,3]").is_err());
        assert!(serde_json::from_str::<ColorConfig>("{\"r\":1,\"g\":2,\"b\":3}").is_err());
        assert!(serde_json::from_str::<ColorConfig>("null").is_err());
        // not valid json at all
        assert!(serde_json::from_str::<ColorConfig>("{").is_err());
    }
}
