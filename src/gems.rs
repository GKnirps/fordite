use crate::height_maps::{height_map_from_config, HeightMap};
use crate::linear_algebra::V3;
use crate::{ColorConfig, Config};
use fastrand::Rng;
use image::{ImageBuffer, ImageError, ImageFormat, Rgba, RgbaImage};
use serde::Deserialize;
use std::cmp::{max, min};
use std::error::Error;
use std::fmt::{Display, Formatter};
use std::num::NonZeroUsize;
use std::path::Path;

#[derive(Clone, Debug, Deserialize)]
#[serde(rename_all = "snake_case", tag = "type", content = "params")]
pub enum LayersConfig {
    HorizontalPlanes(HorizontalPlanesConfig),
    InclinedPlanes(InclinedPlanesConfig),
    HorizontalHalfPlanes,
    Circles,
    CirclePlanes,
    Sphere(SphereConfig),
    DisplayHeightMap,
    Cylinder(CylinderConfig),
    BarberPole(BarberPoleConfig),
}

#[derive(Clone, Debug, Deserialize)]
pub struct HorizontalPlanesConfig {
    layer_thickness: f64,
}

#[derive(Clone, Debug, Deserialize)]
pub struct InclinedPlanesConfig {
    layer_thickness: f64,
}

#[derive(Clone, Debug, Deserialize)]
pub struct SphereConfig {
    center: V3,
    layer_thickness: f64,
}

#[derive(Clone, Debug, Deserialize)]
pub struct CylinderConfig {
    origin: V3,
    orientation: V3,
    layer_thickness: f64,
}

#[derive(Clone, Debug, Deserialize)]
pub struct BarberPoleConfig {
    origin: V3,
    orientation: V3,
    interval: f64,
    num_slices: NonZeroUsize,
    offset: Option<f64>,
}

#[derive(Debug)]
pub enum CreateImgErr {
    Image(String, ImageError),
    Config(String),
}

impl Error for CreateImgErr {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        match self {
            CreateImgErr::Image(_, e) => Some(e),
            CreateImgErr::Config(_) => None,
        }
    }
}

impl Display for CreateImgErr {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            CreateImgErr::Image(msg, e) => write!(f, "{msg}: {e}"),
            CreateImgErr::Config(msg) => f.write_str(msg),
        }
    }
}

pub fn create_image(config: &Config) -> Result<(), CreateImgErr> {
    let mut rng = config
        .rand_seed
        .map(Rng::with_seed)
        .unwrap_or_else(Rng::new);
    let height_map = height_map_from_config(&config.height_map, &config.dimensions, &mut rng);

    let cfg_palette: Vec<Rgba<u8>> = config
        .palette
        .iter()
        .map(|ColorConfig(c)| c)
        .copied()
        .collect();

    let palette: &[Rgba<u8>] = if cfg_palette.is_empty() {
        PALETTE
    } else {
        &cfg_palette
    };
    let img = match &config.layers {
        LayersConfig::HorizontalPlanes(hp_config) => create_horizontal_planes_gem(
            &height_map,
            &mut rng,
            config.dimensions.max_layer,
            palette,
            hp_config,
        ),
        LayersConfig::InclinedPlanes(ip_config) => create_inclined_planes_gem(
            &height_map,
            &mut rng,
            config.dimensions.max_layer,
            palette,
            ip_config,
        ),
        LayersConfig::HorizontalHalfPlanes => create_horizontal_half_planes_gem(
            &height_map,
            &mut rng,
            config.dimensions.max_layer,
            palette,
        ),
        LayersConfig::Circles => {
            create_circle_gem(&height_map, &mut rng, config.dimensions.max_layer, palette)
        }
        LayersConfig::CirclePlanes => {
            create_circle_layers_gem(&height_map, &mut rng, config.dimensions.max_layer, palette)
        }
        LayersConfig::Sphere(sphere_config) => create_sphere_layer_gem(
            &height_map,
            &mut rng,
            config.dimensions.max_layer,
            palette,
            sphere_config,
        ),
        LayersConfig::Cylinder(cyl_config) => create_cylinder_layer_gem(
            &height_map,
            &mut rng,
            config.dimensions.max_layer,
            palette,
            cyl_config,
        ),
        LayersConfig::DisplayHeightMap => {
            render_height_map(&height_map, config.dimensions.max_layer)
        }
        LayersConfig::BarberPole(bp_config) => {
            create_barber_pole_gem(&height_map, &mut rng, palette, bp_config)
        }
    }?;

    let output_file: &Path = config
        .output
        .as_deref()
        .unwrap_or_else(|| Path::new("out.png"));

    img.save_with_format(output_file, ImageFormat::Png)
        .map_err(|e| {
            CreateImgErr::Image(
                format!("unable to write image file '{}'", output_file.display()),
                e,
            )
        })
}

const PALETTE: &[Rgba<u8>] = &[
    Rgba([255, 0, 0, 255]),
    Rgba([0, 255, 0, 255]),
    Rgba([0, 0, 255, 255]),
    Rgba([255, 255, 0, 255]),
    Rgba([127, 0, 0, 255]),
    Rgba([0, 0, 127, 255]),
    Rgba([127, 0, 127, 255]),
];
const TRANSPARENT: Rgba<u8> = Rgba([0u8, 0u8, 0u8, 0u8]);

// colors are horizontal (parallel to the projection plane) layers of paint
// a layer of paint can be thicker than 1
fn create_horizontal_planes_gem(
    height_map: &HeightMap,
    rng: &mut Rng,
    max_height: f64,
    palette: &[Rgba<u8>],
    config: &HorizontalPlanesConfig,
) -> Result<RgbaImage, CreateImgErr> {
    check_layer_thickness(config.layer_thickness)?;
    let num_layers = (max_height / config.layer_thickness).ceil() as u32 + 1;
    let layers = sample_colors(rng, palette, num_layers);

    create_gem(height_map, |_, _, z| {
        if z < 0.0 {
            return TRANSPARENT;
        }
        *layers
            .get((z / config.layer_thickness).round() as usize)
            .or_else(|| layers.last())
            .unwrap_or(&TRANSPARENT)
    })
}

// render a height map from black (lowest point) to white (highest point) in gray values
fn render_height_map(height_map: &HeightMap, max_height: f64) -> Result<RgbaImage, CreateImgErr> {
    create_gem(height_map, |_, _, l| {
        let level: u8 = ((l * 255.0) / max_height).round().max(0.0) as u8;
        Rgba([level, level, level, 255])
    })
}

// colors consist of layers where each layer takes half of the canvas, switching between
// left half, top half, right half, bottom half, in this order, repeatedly
fn create_horizontal_half_planes_gem(
    height_map: &HeightMap,
    rng: &mut Rng,
    max_height: f64,
    palette: &[Rgba<u8>],
) -> Result<RgbaImage, CreateImgErr> {
    let (width, height) = u32_dimensions(height_map)?;

    // TODO: make configurable
    let max_layer_thickness = 2;

    // each quadrant may receive up to twice the required amount of layers due to random thickness
    let quadrant_capacity = (max_height * max_layer_thickness as f64).ceil().max(0.0) as usize;
    let mut topleft: Vec<Rgba<u8>> = Vec::with_capacity(quadrant_capacity);
    let mut topright: Vec<Rgba<u8>> = Vec::with_capacity(quadrant_capacity);
    let mut botleft: Vec<Rgba<u8>> = Vec::with_capacity(quadrant_capacity);
    let mut botright: Vec<Rgba<u8>> = Vec::with_capacity(quadrant_capacity);

    // since every quadrant only gets 1/2 of the layers, we need max_height * 2 iterations here to
    // be sure every quadrant reaches the top
    for i in 0..(max_height.ceil().max(0.0) as u32 * 2) {
        let thickness = rng.u32(1..=max_layer_thickness);
        let color = palette[rng.usize(0..palette.len())];
        let (quarter1, quarter2) = match i % 4 {
            0 => (&mut topleft, &mut botleft),
            1 => (&mut topleft, &mut topright),
            2 => (&mut topright, &mut botright),
            _ => (&mut botleft, &mut botright),
        };
        for _ in 0..thickness {
            quarter1.push(color);
            quarter2.push(color);
        }
    }
    create_gem(height_map, |x, y, z| {
        if z < 0.0 {
            return TRANSPARENT;
        }
        let quarter = if x < width / 2 {
            if y < height / 2 {
                &topleft
            } else {
                &botleft
            }
        } else if y < height / 2 {
            &topright
        } else {
            &botright
        };
        *quarter
            .get(z.round() as usize)
            .or_else(|| quarter.last())
            .unwrap_or(&TRANSPARENT)
    })
}

// circles of different colors are painted on the canvas
// since circles usually only overlap partially, the same circle will stretch over different
// heights
fn create_circle_gem(
    height_map: &HeightMap,
    rng: &mut Rng,
    max_height: f64,
    palette: &[Rgba<u8>],
) -> Result<RgbaImage, CreateImgErr> {
    let (width, height) = u32_dimensions(height_map)?;
    // TODO: make radius configurable and randomizable
    let radius_sq = ((min(height, width) * 2) / 3).pow(2);

    // TODO: max_height * 20 ist just an heuristic. It cannot be guaranteed that this every
    // point reaches the maximum height.
    let circles: Vec<(u32, u32, Rgba<u8>)> = (0..(max_height.ceil().max(0.0) * 20.0) as u32)
        .flat_map(|_| {
            let center_x = rng.u32(0..width);
            let center_y = rng.u32(0..height);
            let color = palette[rng.usize(0..palette.len())];
            (0..rng.u32(1..=2)).map(move |_| (center_x, center_y, color))
        })
        .collect();

    create_gem(height_map, |x, y, z| {
        let z = z.round() as u32;
        let mut layer_count: u32 = 0;
        let mut top_color = TRANSPARENT;
        for (center_x, center_y, color) in &circles {
            top_color = *color;
            if (max(*center_x, x) - min(*center_x, x)).pow(2)
                + (max(*center_y, y) - min(*center_y, y)).pow(2)
                <= radius_sq
            {
                layer_count += 1;
            }
            if layer_count >= z {
                break;
            }
        }
        top_color
    })
}

// each layer consist of a circular shape in one color and a a background color
fn create_circle_layers_gem(
    height_map: &HeightMap,
    rng: &mut Rng,
    max_height: f64,
    palette: &[Rgba<u8>],
) -> Result<RgbaImage, CreateImgErr> {
    let (width, height) = u32_dimensions(height_map)?;
    let radius_sq = ((min(height, width) * 2) / 3).pow(2);
    let layers: Vec<(u32, u32, Rgba<u8>, Rgba<u8>)> = (0..max_height.ceil().max(0.0) as u32)
        .flat_map(|_| {
            let center_x = rng.u32(0..width);
            let center_y = rng.u32(0..height);
            let color = palette[rng.usize(0..palette.len())];
            let other_color = palette[rng.usize(0..palette.len())];
            (0..rng.u32(1..=3)).map(move |_| (center_x, center_y, color, other_color))
        })
        .collect();
    create_gem(height_map, |x, y, z| {
        if z < 0.9 {
            return TRANSPARENT;
        }
        let (center_x, center_y, color, other_color) = layers
            .get(z.round() as usize)
            .or_else(|| layers.last())
            .cloned()
            .unwrap_or((0, 0, TRANSPARENT, TRANSPARENT));
        if (max(center_x, x) - min(center_x, x)).pow(2)
            + (max(center_y, y) - min(center_y, y)).pow(2)
            <= radius_sq
        {
            color
        } else {
            other_color
        }
    })
}

// colors are inclined (to the projection plane) layers of paint
// layers are rising on the x-axis
fn create_inclined_planes_gem(
    height_map: &HeightMap,
    rng: &mut Rng,
    max_height: f64,
    palette: &[Rgba<u8>],
    config: &InclinedPlanesConfig,
) -> Result<RgbaImage, CreateImgErr> {
    check_layer_thickness(config.layer_thickness)?;
    let colors = sample_colors(rng, palette, max_height.ceil().max(0.0) as u32 + 1);

    create_gem(height_map, |x, _, z| {
        let i = x.min(z.round() as u32);
        colors[((x - i) as f64 / config.layer_thickness) as usize % colors.len()]
    })
}

// colors are layered spherical around a center
fn create_sphere_layer_gem(
    height_map: &HeightMap,
    rng: &mut Rng,
    max_height: f64,
    palette: &[Rgba<u8>],
    config: &SphereConfig,
) -> Result<RgbaImage, CreateImgErr> {
    check_layer_thickness(config.layer_thickness)?;
    let (width, height) = u32_dimensions(height_map)?;

    // a simple approximation about the largest radius we need to get to every part of the image
    let diagonal_length = V3::new(width as f64, height as f64, max_height).length();
    let max_radius = diagonal_length + config.center.length();

    let colors = sample_colors(
        rng,
        palette,
        (max_radius / config.layer_thickness.ceil()) as u32 + 1,
    );
    create_gem(height_map, move |x, y, z| {
        let layer = ((config.center - V3::new(x as f64, y as f64, z)).length()
            / config.layer_thickness)
            .round();
        *colors
            .get(layer as usize)
            .or_else(|| colors.last())
            .unwrap_or(&TRANSPARENT)
    })
}

fn create_cylinder_layer_gem(
    height_map: &HeightMap,
    rng: &mut Rng,
    max_height: f64,
    palette: &[Rgba<u8>],
    config: &CylinderConfig,
) -> Result<RgbaImage, CreateImgErr> {
    check_layer_thickness(config.layer_thickness)?;
    let orientation_length = config.orientation.length();
    if orientation_length < f64::EPSILON {
        return Err(CreateImgErr::Config(
            "cylinder orientation must not be a zero vector".to_string(),
        ));
    }
    let orientation = config.orientation / orientation_length;
    // calculate the distance of the center of the cylinder to the origin, so we get an upper limit
    // on how many layers we need
    let distance_origin = (orientation * orientation.scalar_product(config.origin)).length();
    // add the diagonal length to that distance, so no matter where we are, we have enough layers
    let diagonal_length = V3::new(
        height_map.width.get() as f64,
        height_map.height.get() as f64,
        max_height,
    )
    .length();
    let max_distance = distance_origin + diagonal_length;

    let colors = sample_colors(
        rng,
        palette,
        (max_distance / config.layer_thickness.ceil()) as u32,
    );
    create_gem(height_map, |x, y, z| {
        let s = V3::new(x as f64, y as f64, z) - config.origin;
        let projection = orientation * orientation.scalar_product(s);
        let distance = (projection - s).length();
        let layer = (distance / config.layer_thickness).round() as usize;
        *colors
            .get(layer)
            .or_else(|| colors.last())
            .unwrap_or(&TRANSPARENT)
    })
}

fn create_barber_pole_gem(
    height_map: &HeightMap,
    rng: &mut Rng,
    palette: &[Rgba<u8>],
    config: &BarberPoleConfig,
) -> Result<RgbaImage, CreateImgErr> {
    let orientation_length = config.orientation.length();
    if orientation_length < f64::EPSILON {
        return Err(CreateImgErr::Config(
            "barber pole orientation must not be a zero vector".to_string(),
        ));
    }
    if config.interval < f64::EPSILON {
        return Err(CreateImgErr::Config(
            "barber pole interval must be greater than 0".to_string(),
        ));
    }

    let colors = sample_colors(rng, palette, config.num_slices.get() as u32);

    let orientation = config.orientation / orientation_length;

    // we need to find a vector perpendicular to the central straight
    // step 1: find _any_ vector that does not go in the same direction as the straight
    let not_orientation = if orientation.x < f64::EPSILON {
        V3::new(1.0, orientation.y, orientation.z)
    } else if orientation.y < f64::EPSILON {
        V3::new(0.0, 1.0, orientation.z)
    } else {
        V3::new(0.0, orientation.y, orientation.z)
    };
    let perp = orientation.cross_product(not_orientation);
    let perp = perp / perp.length();

    let full_circle = 2.0 * std::f64::consts::PI;

    create_gem(height_map, |x, y, z| {
        let s = V3::new(x as f64, y as f64, z) - config.origin;
        let proj_len = orientation.scalar_product(s);
        let projection = orientation * proj_len;

        let center_to_point = projection - s;
        let center_to_point_length = center_to_point.length();

        let angle = config.offset.unwrap_or(0.0)
            + if center_to_point_length < f64::EPSILON {
                0.0
            } else {
                orientation
                    .scalar_product(center_to_point.cross_product(perp))
                    .signum()
                    * (center_to_point.scalar_product(perp) / center_to_point_length).acos()
            };

        let slice: usize = (((angle + proj_len * full_circle / config.interval)
            .rem_euclid(full_circle)
            / (full_circle))
            * config.num_slices.get() as f64)
            .round() as usize
            % config.num_slices;
        *colors.get(slice).unwrap_or(&TRANSPARENT)
    })
}

fn create_gem<F>(height_map: &HeightMap, color_at: F) -> Result<RgbaImage, CreateImgErr>
where
    F: Fn(u32, u32, f64) -> Rgba<u8>,
{
    let (width, height) = u32_dimensions(height_map)?;
    let mut img: RgbaImage = ImageBuffer::from_pixel(width, height, Rgba([0u8, 0u8, 0u8, 0u8]));
    for (x, y, px) in img.enumerate_pixels_mut() {
        if let Some(z) = height_map.get(x as usize, y as usize) {
            if z >= 0.0 {
                *px = color_at(x, y, z);
            }
        }
    }
    Ok(img)
}

// tries to convert height map dimensions (usize) into u32 (required for image dimensions)
fn u32_dimensions(hm: &HeightMap) -> Result<(u32, u32), CreateImgErr> {
    let width = u32::try_from(hm.width.get())
        .map_err(|_| CreateImgErr::Config(format!("image width to large: {}", hm.width)))?;
    let height = u32::try_from(hm.height.get())
        .map_err(|_| CreateImgErr::Config(format!("image height to large: {}", hm.height)))?;
    Ok((width, height))
}

fn check_layer_thickness(layer_thickness: f64) -> Result<(), CreateImgErr> {
    if layer_thickness < f64::EPSILON {
        Err(CreateImgErr::Config(format!(
            "layer thickness must be greater than 0, is {}",
            layer_thickness
        )))
    } else {
        Ok(())
    }
}

fn sample_colors(rng: &mut Rng, palette: &[Rgba<u8>], n: u32) -> Vec<Rgba<u8>> {
    (0..n)
        .map(|_| palette[rng.usize(0..palette.len())])
        .collect()
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn create_horizontal_planes_gem_creates_gem() {
        // given
        let mut rng = Rng::with_seed(43);
        let max_height = 10.0;
        let height_map = HeightMap {
            width: NonZeroUsize::new(3).unwrap(),
            height: NonZeroUsize::new(2).unwrap(),
            cells: vec![1.0, 1.0, 2.0, 2.0, 5.0, 10.0],
        };
        let palette = &[
            Rgba([255, 0, 0, 255]),
            Rgba([0, 255, 0, 255]),
            Rgba([0, 0, 255, 255]),
        ];
        let hp_config = HorizontalPlanesConfig {
            layer_thickness: 1.0,
        };

        // when
        let img =
            create_horizontal_planes_gem(&height_map, &mut rng, max_height, palette, &hp_config)
                .expect("expected successful creation");

        // then
        assert_eq!(img.width(), 3);
        assert_eq!(img.height(), 2);

        assert_eq!(img.get_pixel(0, 0), &Rgba([255, 0, 0, 255]));
        assert_eq!(img.get_pixel(1, 0), &Rgba([255, 0, 0, 255]));
        assert_eq!(img.get_pixel(2, 0), &Rgba([0, 0, 255, 255]));
        assert_eq!(img.get_pixel(0, 1), &Rgba([0, 0, 255, 255]));
        assert_eq!(img.get_pixel(1, 1), &Rgba([255, 0, 0, 255]));
        assert_eq!(img.get_pixel(2, 1), &Rgba([255, 0, 0, 255]));
    }

    #[test]
    fn create_horizontal_planes_gem_creates_gem_for_thick_layers() {
        // given
        let mut rng = Rng::with_seed(45);
        let max_height = 10.0;
        let height_map = HeightMap {
            width: NonZeroUsize::new(3).unwrap(),
            height: NonZeroUsize::new(2).unwrap(),
            cells: vec![0.0, 1.0, 3.0, 6.0, 9.0, 10.0],
        };
        let palette = &[
            Rgba([255, 0, 0, 255]),
            Rgba([0, 255, 0, 255]),
            Rgba([0, 0, 255, 255]),
        ];

        let hp_config = HorizontalPlanesConfig {
            layer_thickness: 6.0,
        };

        // when
        let img =
            create_horizontal_planes_gem(&height_map, &mut rng, max_height, palette, &hp_config)
                .expect("expected successful creation");

        // then
        assert_eq!(img.width(), 3);
        assert_eq!(img.height(), 2);

        let lower_color = img.get_pixel(0, 0);
        let middle_color = img.get_pixel(0, 1);
        let upper_color = img.get_pixel(2, 1);

        // the RNG is seeded so the colors are different. If that changes, try another seed.
        assert_ne!(lower_color, upper_color);
        assert_ne!(upper_color, middle_color);
        assert_ne!(lower_color, middle_color);
        assert_eq!(img.get_pixel(1, 0), lower_color);
        assert_eq!(img.get_pixel(2, 0), middle_color);
        assert_eq!(img.get_pixel(1, 1), upper_color);
    }

    #[test]
    fn create_horizontal_planes_works_if_layer_thickness_is_higher_than_max_height() {
        // given
        let mut rng = Rng::with_seed(42);
        let max_height = 10.0;
        let height_map = HeightMap {
            width: NonZeroUsize::new(3).unwrap(),
            height: NonZeroUsize::new(2).unwrap(),
            cells: vec![0.0, 1.0, 3.0, 5.0, 9.0, 10.0],
        };
        let palette = &[
            Rgba([255, 0, 0, 255]),
            Rgba([0, 255, 0, 255]),
            Rgba([0, 0, 255, 255]),
        ];

        let hp_config = HorizontalPlanesConfig {
            layer_thickness: 11.0,
        };

        // when
        let img =
            create_horizontal_planes_gem(&height_map, &mut rng, max_height, palette, &hp_config)
                .expect("expected successful creation");

        // then
        assert_eq!(img.width(), 3);
        assert_eq!(img.height(), 2);

        assert_eq!(img.get_pixel(0, 0), &Rgba([0, 0, 255, 255]));
        assert_eq!(img.get_pixel(1, 0), &Rgba([0, 0, 255, 255]));
        assert_eq!(img.get_pixel(2, 0), &Rgba([0, 0, 255, 255]));
        assert_eq!(img.get_pixel(0, 1), &Rgba([0, 0, 255, 255]));
        assert_eq!(img.get_pixel(1, 1), &Rgba([0, 0, 255, 255]));
        assert_eq!(img.get_pixel(2, 1), &Rgba([0, 0, 255, 255]));
    }

    #[test]
    fn create_horizontal_planes_fails_if_layer_thickness_is_0() {
        // given
        let mut rng = Rng::with_seed(45);
        let max_height = 10.0;
        let height_map = HeightMap {
            width: NonZeroUsize::new(3).unwrap(),
            height: NonZeroUsize::new(2).unwrap(),
            cells: vec![0.0, 1.0, 3.0, 6.0, 9.0, 10.0],
        };

        let hp_config = HorizontalPlanesConfig {
            layer_thickness: 0.0,
        };

        // when
        let result =
            create_horizontal_planes_gem(&height_map, &mut rng, max_height, PALETTE, &hp_config);

        // then
        let err = result.expect_err("expected gem creation to fail");

        match err {
            CreateImgErr::Config(msg) => {
                assert_eq!(&msg, "layer thickness must be greater than 0, is 0")
            }
            _ => panic!("unexpected error: {err}"),
        }
    }

    #[test]
    fn create_inclined_planes_gem_creates_gem() {
        // given
        let mut rng = Rng::with_seed(42);
        let max_height = 10.0;
        let height_map = HeightMap {
            width: NonZeroUsize::new(3).unwrap(),
            height: NonZeroUsize::new(2).unwrap(),
            cells: vec![5.0, 5.0, 5.0, 5.0, 5.0, 5.0],
        };
        let palette = &[
            Rgba([255, 0, 0, 255]),
            Rgba([0, 255, 0, 255]),
            Rgba([0, 0, 255, 255]),
        ];

        let config = InclinedPlanesConfig {
            layer_thickness: 1.0,
        };

        // when
        let img = create_inclined_planes_gem(&height_map, &mut rng, max_height, palette, &config)
            .expect("expected successful creation");

        // then
        assert_eq!(img.width(), 3);
        assert_eq!(img.height(), 2);
    }

    #[test]
    fn create_inclined_planes_gem_fails_if_layer_thickness_is_0() {
        // given
        let mut rng = Rng::with_seed(42);
        let max_height = 10.0;
        let height_map = HeightMap {
            width: NonZeroUsize::new(3).unwrap(),
            height: NonZeroUsize::new(2).unwrap(),
            cells: vec![5.0, 5.0, 5.0, 5.0, 5.0, 5.0],
        };

        let config = InclinedPlanesConfig {
            layer_thickness: 0.0,
        };

        // when
        let result =
            create_inclined_planes_gem(&height_map, &mut rng, max_height, PALETTE, &config);

        // then
        let err = result.expect_err("expected failure");
        match err {
            CreateImgErr::Config(msg) => {
                assert_eq!(msg, "layer thickness must be greater than 0, is 0")
            }
            _ => panic!("unexpected error: {err}"),
        }
    }

    #[test]
    fn create_sphere_layer_gem_fails_if_layer_thickness_is_0() {
        // given
        let mut rng = Rng::with_seed(42);
        let max_height = 10.0;
        let height_map = HeightMap {
            width: NonZeroUsize::new(3).unwrap(),
            height: NonZeroUsize::new(2).unwrap(),
            cells: vec![5.0, 5.0, 5.0, 5.0, 5.0, 5.0],
        };

        let config = SphereConfig {
            center: V3::new(0.0, 0.0, 0.0),
            layer_thickness: 0.0,
        };

        // when
        let result = create_sphere_layer_gem(&height_map, &mut rng, max_height, PALETTE, &config);

        // then
        let err = result.expect_err("expected failure");
        match err {
            CreateImgErr::Config(msg) => {
                assert_eq!(msg, "layer thickness must be greater than 0, is 0")
            }
            _ => panic!("unexpected error: {err}"),
        }
    }

    #[test]
    fn create_cylinder_layer_gem_fails_if_layer_thickness_is_0() {
        // given
        let mut rng = Rng::with_seed(42);
        let max_height = 10.0;
        let height_map = HeightMap {
            width: NonZeroUsize::new(3).unwrap(),
            height: NonZeroUsize::new(2).unwrap(),
            cells: vec![5.0, 5.0, 5.0, 5.0, 5.0, 5.0],
        };

        let config = CylinderConfig {
            origin: V3::new(0.0, 0.0, 0.0),
            orientation: V3::new(1.0, 0.0, 0.0),
            layer_thickness: 0.0,
        };

        // when
        let result = create_cylinder_layer_gem(&height_map, &mut rng, max_height, PALETTE, &config);

        // then
        let err = result.expect_err("expected failure");
        match err {
            CreateImgErr::Config(msg) => {
                assert_eq!(msg, "layer thickness must be greater than 0, is 0")
            }
            _ => panic!("unexpected error: {err}"),
        }
    }

    #[test]
    fn create_cylinder_layer_gem_fails_if_orientation_is_zero_vector() {
        // given
        let mut rng = Rng::with_seed(42);
        let max_height = 10.0;
        let height_map = HeightMap {
            width: NonZeroUsize::new(3).unwrap(),
            height: NonZeroUsize::new(2).unwrap(),
            cells: vec![5.0, 5.0, 5.0, 5.0, 5.0, 5.0],
        };

        let config = CylinderConfig {
            origin: V3::new(0.0, 0.0, 0.0),
            orientation: V3::new(0.0, 0.0, 0.0),
            layer_thickness: 1.0,
        };

        // when
        let result = create_cylinder_layer_gem(&height_map, &mut rng, max_height, PALETTE, &config);

        // then
        let err = result.expect_err("expected failure");
        match err {
            CreateImgErr::Config(msg) => {
                assert_eq!(msg, "cylinder orientation must not be a zero vector")
            }
            _ => panic!("unexpected error: {err}"),
        }
    }

    #[test]
    fn create_barber_pole_gem_fails_if_orientation_is_zero_vector() {
        // given
        let mut rng = Rng::with_seed(42);
        let height_map = HeightMap {
            width: NonZeroUsize::new(3).unwrap(),
            height: NonZeroUsize::new(2).unwrap(),
            cells: vec![5.0, 5.0, 5.0, 5.0, 5.0, 5.0],
        };

        let config = BarberPoleConfig {
            origin: V3::new(0.0, 0.0, 0.0),
            orientation: V3::new(0.0, 0.0, 0.0),
            interval: 1.0,
            num_slices: NonZeroUsize::new(2).unwrap(),
            offset: None,
        };

        // when
        let result = create_barber_pole_gem(&height_map, &mut rng, PALETTE, &config);

        // then
        let err = result.expect_err("expected failure");
        match err {
            CreateImgErr::Config(msg) => {
                assert_eq!(msg, "barber pole orientation must not be a zero vector")
            }
            _ => panic!("unexpected error: {err}"),
        }
    }

    #[test]
    fn create_barber_pole_gem_fails_if_interval_is_0() {
        // given
        let mut rng = Rng::with_seed(42);
        let height_map = HeightMap {
            width: NonZeroUsize::new(3).unwrap(),
            height: NonZeroUsize::new(2).unwrap(),
            cells: vec![5.0, 5.0, 5.0, 5.0, 5.0, 5.0],
        };

        let config = BarberPoleConfig {
            origin: V3::new(0.0, 0.0, 0.0),
            orientation: V3::new(1.0, 0.0, 0.0),
            interval: 0.0,
            num_slices: NonZeroUsize::new(2).unwrap(),
            offset: None,
        };

        // when
        let result = create_barber_pole_gem(&height_map, &mut rng, PALETTE, &config);

        // then
        let err = result.expect_err("expected failure");
        match err {
            CreateImgErr::Config(msg) => {
                assert_eq!(msg, "barber pole interval must be greater than 0")
            }
            _ => panic!("unexpected error: {err}"),
        }
    }

    #[test]
    fn sample_colors_samples_random_colors_from_palette() {
        // given
        let mut rng = Rng::with_seed(44);
        let palette = &[
            Rgba([255, 0, 0, 255]),
            Rgba([0, 255, 0, 255]),
            Rgba([0, 0, 255, 255]),
        ];
        let n = 6;

        // when
        let samples = sample_colors(&mut rng, palette, n);

        // then
        assert_eq!(
            &samples,
            &[
                Rgba([0, 0, 255, 255]),
                Rgba([0, 0, 255, 255]),
                Rgba([255, 0, 0, 255]),
                Rgba([0, 0, 255, 255]),
                Rgba([0, 255, 0, 255]),
                Rgba([0, 255, 0, 255]),
            ]
        );
    }
}
