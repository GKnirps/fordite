use fastrand::Rng;
use serde::Deserialize;
use std::num::NonZeroUsize;

#[derive(Clone, Debug, Deserialize)]
pub struct Dimensions {
    pub width: NonZeroUsize,
    pub height: NonZeroUsize,
    pub max_layer: f64,
}

#[derive(Clone, PartialEq, PartialOrd, Debug, Default, Deserialize)]
pub struct Transformation {
    #[serde(default)]
    orientation: f64,
    #[serde(default)]
    translation_x: f64,
    #[serde(default)]
    translation_y: f64,
}

#[derive(Clone, Debug, Deserialize)]
#[serde(rename_all = "snake_case", tag = "type", content = "params")]
pub enum HeightMapConfig {
    Slope(SlopeConfig),
    EllipticParaboloid(EllipticParaboloidConfig),
    HyperbolicParaboloid(HyperbolicParaboloidConfig),
    Sine(SineConfig),
    PolarSine(PolarSineConfig),
    Flat,
    DiamondSquare(DiamondSquareConfig),
    Sum(Vec<WeightedHeightMapConfig>),
}

const fn float_1() -> f64 {
    1.0
}

#[derive(Clone, Debug, Deserialize)]
pub struct WeightedHeightMapConfig {
    #[serde(default = "float_1")]
    weight: f64,
    #[serde(flatten)]
    config: HeightMapConfig,
}

#[derive(Clone, Debug, Deserialize)]
pub struct SlopeConfig {
    #[serde(flatten, default)]
    transformation: Transformation,
}

#[derive(Clone, Debug, Deserialize)]
pub struct EllipticParaboloidConfig {
    #[serde(flatten, default)]
    transformation: Transformation,
}

#[derive(Clone, Debug, Deserialize)]
pub struct HyperbolicParaboloidConfig {
    #[serde(flatten, default)]
    transformation: Transformation,
}

#[derive(Clone, PartialEq, PartialOrd, Debug, Deserialize)]
pub struct SineConfig {
    #[serde(default)]
    orientation: f64,
    #[serde(default)]
    phase_offset: f64,
    period: f64,
}

#[derive(Clone, Debug, Deserialize)]
pub struct PolarSineConfig {
    #[serde(flatten, default)]
    transformation: Transformation,
    period: f64,
    phase_offset: Option<f64>,
}

#[derive(Clone, Debug, Deserialize)]
pub struct DiamondSquareConfig {
    smoothness: f64,
    rand_seed: Option<u64>,
}

#[derive(Clone, PartialEq, PartialOrd, Debug)]
pub struct HeightMap {
    pub width: NonZeroUsize,
    pub height: NonZeroUsize,
    pub cells: Vec<f64>,
}

impl HeightMap {
    fn from_fn<F: Fn(f64, f64) -> f64>(
        dimensions: &Dimensions,
        &Transformation {
            orientation,
            translation_x,
            translation_y,
        }: &Transformation,
        px: F,
    ) -> HeightMap {
        let &Dimensions { width, height, .. } = dimensions;
        let mut cells: Vec<f64> = Vec::with_capacity(width.get() * height.get());
        let rot_11 = orientation.cos();
        let rot_22 = rot_11;
        let rot_12 = orientation.sin();
        let rot_21 = -rot_12;
        for y in 0..height.get() {
            for x in 0..width.get() {
                let hm_x = (rot_11 * x as f64 + rot_12 * y as f64) - translation_x;
                let hm_y = (rot_21 * x as f64 + rot_22 * y as f64) - translation_y;
                cells.push(px(hm_x, hm_y))
            }
        }
        HeightMap {
            cells: normalize_cells(&cells, dimensions.max_layer),
            width,
            height,
        }
    }

    pub fn get(&self, x: usize, y: usize) -> Option<f64> {
        if x >= self.width.get() || y >= self.height.get() {
            None
        } else {
            self.cells.get(x + y * self.width.get()).copied()
        }
    }
}

fn normalize_cells(cells: &[f64], max_height: f64) -> Vec<f64> {
    let min = cells
        .iter()
        .copied()
        .filter(|h| !h.is_nan() && h.is_finite())
        .reduce(f64::min)
        .unwrap_or(0.0);
    let max = cells
        .iter()
        .copied()
        .filter(|h| !h.is_nan() && h.is_finite())
        .reduce(f64::max)
        .unwrap_or(1.0);
    if max - min <= f64::EPSILON {
        vec![max_height; cells.len()]
    } else {
        cells
            .iter()
            .map(|h| {
                if h.is_nan() || (h.is_infinite() && h.is_sign_negative()) {
                    0.0
                } else if h.is_infinite() {
                    max_height
                } else {
                    ((h - min) / (max - min)) * max_height
                }
            })
            .collect()
    }
}

pub fn height_map_from_config(
    config: &HeightMapConfig,
    dimensions: &Dimensions,
    rng: &mut Rng,
) -> HeightMap {
    match &config {
        HeightMapConfig::EllipticParaboloid(ep_config) => {
            elliptic_paraboloid_height_map(dimensions, ep_config)
        }
        HeightMapConfig::HyperbolicParaboloid(hp_config) => {
            hyperbolic_paraboloid_height_map(dimensions, hp_config)
        }
        HeightMapConfig::Slope(slope_config) => slope_height_map(dimensions, slope_config),
        HeightMapConfig::Sine(sine_config) => sine_height_map(dimensions, sine_config),
        HeightMapConfig::PolarSine(sine_config) => polar_sine_height_map(dimensions, sine_config),
        HeightMapConfig::Flat => flat_height_map(dimensions),
        HeightMapConfig::DiamondSquare(ds_config) => {
            diamond_square_height_map(dimensions, ds_config, rng)
        }
        HeightMapConfig::Sum(configs) => height_map_sum(dimensions, configs, rng),
    }
}

fn height_map_sum(
    dimensions: &Dimensions,
    configs: &[WeightedHeightMapConfig],
    rng: &mut Rng,
) -> HeightMap {
    let base = HeightMap {
        cells: vec![0.0; dimensions.width.get() * dimensions.height.get()],
        width: dimensions.width,
        height: dimensions.height,
    };
    let mut height_map = configs
        .iter()
        .map(|WeightedHeightMapConfig { config, weight }| {
            height_map_from_config(
                config,
                &Dimensions {
                    width: dimensions.width,
                    height: dimensions.height,
                    max_layer: *weight,
                },
                rng,
            )
        })
        .fold(base, |mut sum, latest| {
            // TODO: maybe we want better handling for invalid cases here.
            //  on the other hand: different width and height are a programming error here since we explicitly
            //  made them all the same width and height
            assert_eq!(sum.width, latest.width);
            assert_eq!(sum.height, latest.height);
            assert_eq!(sum.cells.len(), latest.cells.len());
            for (i, cell) in sum.cells.iter_mut().enumerate() {
                *cell += latest.cells[i];
            }
            sum
        });
    height_map.cells = normalize_cells(&height_map.cells, dimensions.max_layer);
    height_map
}

fn elliptic_paraboloid_height_map(
    dimensions: &Dimensions,
    config: &EllipticParaboloidConfig,
) -> HeightMap {
    HeightMap::from_fn(dimensions, &config.transformation, |x, y| {
        -elliptic_paraboloid(x, y)
    })
}

fn elliptic_paraboloid(x: f64, y: f64) -> f64 {
    x.powi(2) + y.powi(2)
}

fn hyperbolic_paraboloid_height_map(
    dimensions: &Dimensions,
    config: &HyperbolicParaboloidConfig,
) -> HeightMap {
    HeightMap::from_fn(dimensions, &config.transformation, hyperbolic_paraboloid)
}

fn hyperbolic_paraboloid(x: f64, y: f64) -> f64 {
    x.powi(2) - y.powi(2)
}

fn sine_height_map(dimensions: &Dimensions, sine_config: &SineConfig) -> HeightMap {
    HeightMap::from_fn(
        dimensions,
        &Transformation {
            orientation: sine_config.orientation,
            ..Transformation::default()
        },
        |x, _| {
            (x * 2.0 * std::f64::consts::PI / sine_config.period + sine_config.phase_offset).sin()
        },
    )
}

fn polar_sine_height_map(dimensions: &Dimensions, sine_config: &PolarSineConfig) -> HeightMap {
    let phase_offset = sine_config.phase_offset.unwrap_or(0.0);
    HeightMap::from_fn(dimensions, &sine_config.transformation, |x, y| {
        ((x.powi(2) + y.powi(2)).sqrt() * 2.0 * std::f64::consts::PI / sine_config.period
            + phase_offset)
            .sin()
    })
}

fn slope_height_map(dimensions: &Dimensions, config: &SlopeConfig) -> HeightMap {
    HeightMap::from_fn(dimensions, &config.transformation, |x, _| x)
}

fn flat_height_map(dimensions: &Dimensions) -> HeightMap {
    HeightMap::from_fn(dimensions, &Transformation::default(), |_, _| 1.0)
}

fn diamond_square_height_map(
    dimensions: &Dimensions,
    ds_config: &DiamondSquareConfig,
    rng: &mut Rng,
) -> HeightMap {
    // TODO: return an error if dimensions are too large
    let size = (dimensions.width.max(dimensions.height))
        .checked_next_power_of_two()
        .expect("height map size too large")
        .get();

    // only use provided RNG if we do not have our own seed
    let mut own_rng = ds_config.rand_seed.map(Rng::with_seed);
    let rng: &mut Rng = own_rng.as_mut().unwrap_or(rng);

    let mut landscape: Vec<f64> = vec![0.0; size * size];

    let mut square_size = size;
    let mut rand_magnitude: f64 = 1.0;
    while square_size > 1 {
        // diamond step
        for y in 0..(size / square_size) {
            for x in 0..(size / square_size) {
                diamond_step(
                    &mut landscape,
                    size,
                    square_size,
                    x * square_size,
                    y * square_size,
                    rng,
                    rand_magnitude,
                );
            }
        }
        // square step
        for y in 0..(size / square_size) {
            for x in 0..(size / square_size) {
                square_step(
                    &mut landscape,
                    size,
                    square_size / 2,
                    square_size / 2 + square_size * x,
                    square_size * y,
                    rng,
                    rand_magnitude,
                );
                square_step(
                    &mut landscape,
                    size,
                    square_size / 2,
                    square_size * x,
                    square_size / 2 + square_size * y,
                    rng,
                    rand_magnitude,
                );
            }
        }

        square_size /= 2;
        rand_magnitude *= 2.0_f64.powf(-ds_config.smoothness);
    }

    // the actual height map we want is possibly smaller, so we just cut off the edges
    let mut cells: Vec<f64> = vec![0.0; dimensions.width.get() * dimensions.height.get()];
    for y in 0..dimensions.height.get() {
        for x in 0..dimensions.width.get() {
            cells[x + y * dimensions.width.get()] = landscape[x + y * size];
        }
    }
    HeightMap {
        cells: normalize_cells(&cells, dimensions.max_layer),
        width: dimensions.width,
        height: dimensions.height,
    }
}

fn diamond_step(
    landscape: &mut [f64],
    total_size: usize,
    sub_square_size: usize,
    offset_x: usize,
    offset_y: usize,
    rng: &mut Rng,
    rand_magnitude: f64,
) {
    let x = offset_x + sub_square_size / 2;
    let y = offset_y + sub_square_size / 2;

    landscape[x + total_size * y] = (landscape[offset_x + total_size * offset_y]
        + landscape[(offset_x + sub_square_size) % total_size + total_size * offset_y]
        + landscape[offset_x + ((sub_square_size + offset_y) % total_size) * total_size]
        + landscape[(offset_x + sub_square_size) % total_size
            + ((sub_square_size + offset_y) % total_size) * total_size])
        / 4.0
        + (rng.f64() * rand_magnitude * 2.0)
        - rand_magnitude;
}

fn square_step(
    landscape: &mut [f64],
    total_size: usize,
    d: usize,
    x: usize,
    y: usize,
    rng: &mut Rng,
    rand_magnitude: f64,
) {
    let neighbours: [(usize, usize); 4] = [
        ((x + d) % total_size, y),
        (x, (y + d) % total_size),
        if x == 0 {
            (total_size - d, y)
        } else {
            (x - d, y)
        },
        if y == 0 {
            (x, total_size - d)
        } else {
            (x, y - d)
        },
    ];
    landscape[x + total_size * y] = neighbours
        .iter()
        .map(|(x, y)| landscape[x + y * total_size])
        .sum::<f64>()
        / 4.0
        + (rng.f64() * rand_magnitude * 2.0)
        - rand_magnitude;
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn height_map_creates_from_fn() {
        // given
        let dimensions = Dimensions {
            width: NonZeroUsize::new(3).unwrap(),
            height: NonZeroUsize::new(4).unwrap(),
            max_layer: 32.0,
        };
        let transformation = Transformation::default();

        // when
        let result = HeightMap::from_fn(&dimensions, &transformation, |x, y| x + y * 10.0);

        // then
        assert_eq!(result.width.get(), 3);
        assert_eq!(result.height.get(), 4);
        assert_eq!(
            &result.cells,
            &[0.0, 1.0, 2.0, 10.0, 11.0, 12.0, 20.0, 21.0, 22.0, 30.0, 31.0, 32.0]
        );
    }

    #[test]
    fn height_map_get_returns_correct_values() {
        // given
        let hm = HeightMap {
            width: NonZeroUsize::new(2).unwrap(),
            height: NonZeroUsize::new(3).unwrap(),
            cells: vec![0.0, 1.0, 2.0, 3.0, 4.0, 5.0],
        };

        // when/then
        assert_eq!(hm.get(0, 0), Some(0.0));
        assert_eq!(hm.get(1, 0), Some(1.0));
        assert_eq!(hm.get(0, 2), Some(4.0));
        assert_eq!(hm.get(1, 2), Some(5.0));
        assert_eq!(hm.get(2, 0), None);
        assert_eq!(hm.get(0, 3), None);
    }

    #[test]
    fn height_map_sum_creates_and_adds_height_maps() {
        // given
        let dimensions = Dimensions {
            width: NonZeroUsize::new(5).unwrap(),
            height: NonZeroUsize::new(5).unwrap(),
            max_layer: 42.0,
        };
        let config = &[
            WeightedHeightMapConfig {
                weight: 2.0,
                config: HeightMapConfig::Slope(SlopeConfig {
                    transformation: Transformation::default(),
                }),
            },
            WeightedHeightMapConfig {
                weight: 1.0,
                config: HeightMapConfig::EllipticParaboloid(EllipticParaboloidConfig {
                    transformation: Transformation {
                        translation_x: 2.0,
                        translation_y: 2.0,
                        orientation: 0.0,
                    },
                }),
            },
        ];
        let mut rng = Rng::with_seed(42);

        // when
        let hm = height_map_sum(&dimensions, config, &mut rng);

        // then
        assert_eq!(hm.width.get(), 5);
        assert_eq!(hm.height.get(), 5);
        assert_eq!(
            &hm.cells,
            &[
                0.0,
                14.7,
                25.2,
                31.5,
                33.6,
                6.3,
                21.0,
                31.5,
                37.800000000000004,
                39.9,
                8.4,
                23.1,
                33.6,
                39.9,
                42.0,
                6.3,
                21.0,
                31.5,
                37.800000000000004,
                39.9,
                0.0,
                14.7,
                25.2,
                31.5,
                33.6
            ]
        );
    }

    #[test]
    fn flat_height_map_returns_max_layer_height_map() {
        // given
        let dimensions = Dimensions {
            width: NonZeroUsize::new(2).unwrap(),
            height: NonZeroUsize::new(3).unwrap(),
            max_layer: 42.0,
        };

        // when
        let hm = flat_height_map(&dimensions);

        // then
        assert_eq!(
            hm,
            HeightMap {
                width: NonZeroUsize::new(2).unwrap(),
                height: NonZeroUsize::new(3).unwrap(),
                cells: vec![42.0, 42.0, 42.0, 42.0, 42.0, 42.0]
            }
        );
    }

    #[test]
    fn diamond_square_height_map_generates_valid_height_map() {
        // given
        let mut rng = Rng::with_seed(42);
        let ds_cfg = DiamondSquareConfig {
            smoothness: 0.5,
            rand_seed: None,
        };
        let dimensions = Dimensions {
            width: NonZeroUsize::new(5).unwrap(),
            height: NonZeroUsize::new(3).unwrap(),
            max_layer: 256.0,
        };

        // when
        let hm = diamond_square_height_map(&dimensions, &ds_cfg, &mut rng);

        // then
        assert_eq!(hm.height, dimensions.height);
        assert_eq!(hm.width, dimensions.width);
        for (i, cell) in hm.cells.iter().enumerate() {
            println!("checking cell {i}: {cell}");
            assert!(*cell >= 0.0 && *cell <= 256.0);
        }
        assert!(
            hm.cells.iter().find(|c| **c > 255.0).is_some(),
            "no cell with high height"
        );
    }

    #[test]
    fn diamond_square_height_map_uses_random_seed_if_provided() {
        // given
        let mut rng = Rng::with_seed(100);
        let ds_cfg = DiamondSquareConfig {
            smoothness: 0.5,
            rand_seed: Some(42),
        };
        let dimensions = Dimensions {
            width: NonZeroUsize::new(5).unwrap(),
            height: NonZeroUsize::new(3).unwrap(),
            max_layer: 256.0,
        };

        // when
        let hm = diamond_square_height_map(&dimensions, &ds_cfg, &mut rng);

        // then
        // if `rng` had been used, this would be a different value
        assert_eq!(rng.u64(..), 8402676956540909994);
    }

    #[test]
    fn diamond_step_works_for_largest_square_size() {
        // given
        let size = 4;
        let square_size = 4;
        let x = 0;
        let y = 0;
        let landscape: &mut [f64] = &mut [
            1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        ];
        let mut rng = Rng::with_seed(42);
        let rand_magnitude = 1.0;

        // when
        diamond_step(landscape, size, square_size, x, y, &mut rng, rand_magnitude);

        // then
        assert_eq!(
            landscape,
            &[
                1.0,
                0.0,
                0.0,
                0.0,
                0.0,
                0.0,
                0.0,
                0.0,
                0.0,
                0.0,
                1.3616481720776625,
                0.0,
                0.0,
                0.0,
                0.0,
                0.0
            ]
        );
    }

    #[test]
    fn diamond_step_works_for_smallest_square_size() {
        // given
        let size = 4;
        let square_size = 2;
        let landscape: &mut [f64] = &mut [
            1.0, 0.0, 2.0, 0.0, 0.0, 0.0, 0.0, 0.0, 3.0, 0.0, 4.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        ];
        let mut rng = Rng::with_seed(42);
        // set the random magnitude to 0 so we can check if the mean is calculated correctly
        let rand_magnitude = 0.0;

        // when / then
        diamond_step(landscape, size, square_size, 0, 0, &mut rng, rand_magnitude);
        assert_eq!(
            landscape,
            &[1.0, 0.0, 2.0, 0.0, 0.0, 2.5, 0.0, 0.0, 3.0, 0.0, 4.0, 0.0, 0.0, 0.0, 0.0, 0.0,]
        );
        diamond_step(landscape, size, square_size, 0, 2, &mut rng, rand_magnitude);
        assert_eq!(
            landscape,
            &[1.0, 0.0, 2.0, 0.0, 0.0, 2.5, 0.0, 0.0, 3.0, 0.0, 4.0, 0.0, 0.0, 2.5, 0.0, 0.0,]
        );
        diamond_step(landscape, size, square_size, 2, 0, &mut rng, rand_magnitude);
        assert_eq!(
            landscape,
            &[1.0, 0.0, 2.0, 0.0, 0.0, 2.5, 0.0, 2.5, 3.0, 0.0, 4.0, 0.0, 0.0, 2.5, 0.0, 0.0,]
        );
        diamond_step(landscape, size, square_size, 2, 2, &mut rng, rand_magnitude);
        assert_eq!(
            landscape,
            &[1.0, 0.0, 2.0, 0.0, 0.0, 2.5, 0.0, 2.5, 3.0, 0.0, 4.0, 0.0, 0.0, 2.5, 0.0, 2.5,]
        );
    }

    #[test]
    fn square_step_works_for_central_point() {
        // given
        let size = 4;
        let d = 1;
        let landscape: &mut [f64] = &mut [
            1.0, 0.0, 2.0, 0.0, 0.0, 3.0, 0.0, 4.0, 5.0, 0.0, 6.0, 0.0, 0.0, 7.0, 0.0, 8.0,
        ];
        let mut rng = Rng::with_seed(42);
        // set the random magnitude to 0 so we can check if the mean is calculated correctly
        let rand_magnitude = 0.0;
        let x = 1;
        let y = 2;

        // when
        square_step(landscape, size, d, x, y, &mut rng, rand_magnitude);

        // then
        assert_eq!(
            landscape,
            &[1.0, 0.0, 2.0, 0.0, 0.0, 3.0, 0.0, 4.0, 5.0, 5.25, 6.0, 0.0, 0.0, 7.0, 0.0, 8.0,]
        );
    }

    #[test]
    fn square_step_adds_random_value() {
        // given
        let size = 4;
        let d = 1;
        let landscape: &mut [f64] = &mut [
            1.0, 0.0, 2.0, 0.0, 0.0, 3.0, 0.0, 4.0, 5.0, 0.0, 6.0, 0.0, 0.0, 7.0, 0.0, 8.0,
        ];
        let mut rng = Rng::with_seed(42);
        let rand_magnitude = 1.0;
        let x = 1;
        let y = 2;

        // when
        square_step(landscape, size, d, x, y, &mut rng, rand_magnitude);

        // then
        assert_eq!(
            landscape,
            &[
                1.0,
                0.0,
                2.0,
                0.0,
                0.0,
                3.0,
                0.0,
                4.0,
                5.0,
                5.611648172077663,
                6.0,
                0.0,
                0.0,
                7.0,
                0.0,
                8.0,
            ]
        );
    }

    #[test]
    fn square_step_works_for_left_and_bottom_border() {
        // given
        let size = 4;
        let d = 2;
        let landscape: &mut [f64] = &mut [
            1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 7.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        ];
        let mut rng = Rng::with_seed(42);
        // set the random magnitude to 0 so we can check if the mean is calculated correctly
        let rand_magnitude = 0.0;
        let x = 0;
        let y = 2;

        // when
        square_step(landscape, size, d, x, y, &mut rng, rand_magnitude);

        // then
        assert_eq!(
            landscape,
            &[1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 4.0, 0.0, 7.0, 0.0, 0.0, 0.0, 0.0, 0.0,]
        );
    }

    #[test]
    fn square_step_works_for_right_and_top_border() {
        // given
        let size = 4;
        let d = 2;
        let landscape: &mut [f64] = &mut [
            1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 7.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        ];
        let mut rng = Rng::with_seed(42);
        // set the random magnitude to 0 so we can check if the mean is calculated correctly
        let rand_magnitude = 0.0;
        let x = 2;
        let y = 0;

        // when
        square_step(landscape, size, d, x, y, &mut rng, rand_magnitude);

        // then
        assert_eq!(
            landscape,
            &[1.0, 0.0, 4.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 7.0, 0.0, 0.0, 0.0, 0.0, 0.0,]
        );
    }
}
