use serde::Deserialize;
use std::ops::{Add, Div, Mul, Neg, Sub};

#[derive(Clone, Copy, Debug, Deserialize)]
pub struct V3 {
    pub x: f64,
    pub y: f64,
    pub z: f64,
}

impl V3 {
    #[inline]
    pub fn new(x: f64, y: f64, z: f64) -> Self {
        V3 { x, y, z }
    }

    #[inline]
    pub fn scalar_product(
        self,
        V3 {
            x: xb,
            y: yb,
            z: zb,
        }: Self,
    ) -> f64 {
        self.x * xb + self.y * yb + self.z * zb
    }

    #[inline]
    pub fn length(self) -> f64 {
        (self.x.powi(2) + self.y.powi(2) + self.z.powi(2)).sqrt()
    }

    #[inline]
    pub fn cross_product(
        self,
        V3 {
            x: xb,
            y: yb,
            z: zb,
        }: Self,
    ) -> V3 {
        V3 {
            x: self.y * zb - self.z * yb,
            y: self.z * xb - self.x * zb,
            z: self.x * yb - self.y * xb,
        }
    }
}

impl Add<V3> for V3 {
    type Output = V3;

    #[inline]
    fn add(
        self,
        V3 {
            x: xb,
            y: yb,
            z: zb,
        }: V3,
    ) -> Self::Output {
        V3 {
            x: self.x + xb,
            y: self.y + yb,
            z: self.z + zb,
        }
    }
}

impl Sub<V3> for V3 {
    type Output = V3;

    #[inline]
    fn sub(
        self,
        V3 {
            x: xb,
            y: yb,
            z: zb,
        }: V3,
    ) -> Self::Output {
        V3 {
            x: self.x - xb,
            y: self.y - yb,
            z: self.z - zb,
        }
    }
}

impl Neg for V3 {
    type Output = V3;

    #[inline]
    fn neg(self) -> Self::Output {
        V3 {
            x: -self.x,
            y: -self.y,
            z: -self.z,
        }
    }
}

impl Mul<f64> for V3 {
    type Output = V3;

    #[inline]
    fn mul(self, factor: f64) -> Self::Output {
        V3 {
            x: self.x * factor,
            y: self.y * factor,
            z: self.z * factor,
        }
    }
}

impl Div<f64> for V3 {
    type Output = V3;

    #[inline]
    fn div(self, divisor: f64) -> Self::Output {
        V3 {
            x: self.x / divisor,
            y: self.y / divisor,
            z: self.z / divisor,
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_scalar_product() {
        assert_eq!(
            V3::new(1.0, 2.0, 3.0).scalar_product(V3::new(1.0, 10.0, 100.0)),
            321.0
        );
    }

    #[test]
    fn test_length() {
        assert_eq!(V3::new(2.0, 0.0, 0.0).length(), 2.0);
        assert_eq!(V3::new(0.0, 2.0, 0.0).length(), 2.0);
        assert_eq!(V3::new(0.0, 0.0, 2.0).length(), 2.0);
        assert_eq!(V3::new(1.0, 4.0, 8.0).length(), 9.0);
    }

    #[test]
    fn test_cross_product() {
        // given
        let a = V3::new(1.0, 2.0, 3.0);
        let b = V3::new(-7.0, 8.0, 9.0);

        // when
        let c = a.cross_product(b);

        // then
        assert_eq!(c.x, -6.0);
        assert_eq!(c.y, -30.0);
        assert_eq!(c.z, 22.0);
    }

    #[test]
    fn test_add() {
        // given
        let a = V3::new(1.0, 2.0, 3.0);
        let b = V3::new(10.0, 20.0, 30.0);

        // when
        let c = a + b;

        // then
        assert_eq!(c.x, 11.0);
        assert_eq!(c.y, 22.0);
        assert_eq!(c.z, 33.0);
    }

    #[test]
    fn test_sub() {
        // given
        let a = V3::new(11.0, 22.0, 33.0);
        let b = V3::new(10.0, 20.0, 30.0);

        // when
        let c = a - b;

        // then
        assert_eq!(c.x, 1.0);
        assert_eq!(c.y, 2.0);
        assert_eq!(c.z, 3.0);
    }

    #[test]
    fn test_neg() {
        // given
        let a = V3::new(1.0, 2.0, 3.0);

        // when
        let c = -a;

        // then
        assert_eq!(c.x, -1.0);
        assert_eq!(c.y, -2.0);
        assert_eq!(c.z, -3.0);
    }

    #[test]
    fn test_mul() {
        // given
        let a = V3::new(1.0, 2.0, 3.0);
        let b = 10.0;

        // when
        let c = a * b;

        // then
        assert_eq!(c.x, 10.0);
        assert_eq!(c.y, 20.0);
        assert_eq!(c.z, 30.0);
    }

    #[test]
    fn test_div() {
        // given
        let a = V3::new(10.0, 20.0, 30.0);
        let b = 10.0;

        // when
        let c = a / b;

        // then
        assert_eq!(c.x, 1.0);
        assert_eq!(c.y, 2.0);
        assert_eq!(c.z, 3.0);
    }

    #[test]
    fn v3_deserializes_correctly() {
        // given
        let json = "[1,2,3.0]";

        // when
        let result = serde_json::from_str::<V3>(json);

        // then
        let v = result.expect("expected successful parsing");
        assert_eq!(v.x, 1.0);
        assert_eq!(v.y, 2.0);
        assert_eq!(v.z, 3.0);
    }

    #[test]
    fn v3_fails_deserializing_if_array_has_wrong_length() {
        serde_json::from_str::<V3>("[1,2]").expect_err("expected failed for short array");
        serde_json::from_str::<V3>("[1,2,3,4]").expect_err("expected failed for long array");
    }
}
