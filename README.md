Fordite
=======

This code simulates [fordite](https://en.wikipedia.org/wiki/Fordite) (sometimes called ”motor agate“).

Actual fordite is a gemstone-like artificial stone made of multiple layers of enamel car paint. They were a waste product from the automobile industry, and at some point someone discovered that they can look rather nice if polished like a gem.

Depending on how the layers built up and how it is polished, fordite has really nice agate-like patterns.

This program intends to replicate the layer-and-polishing process and produces images of a projection of the polished object.

Stability
---------

This is a development version. Anything can break between versions. This includes configuration files. The same configuration file may not create the same image in another version or **may not be readable at all due to changes**.

Usage
-----

To run fordite, simply call

```cargo run --release <config file> <config file> [...]```

where `<config file>` is the name of a valid config file (see below). Fordite takes an arbitrary amount of config files.

**WARNING**: Config files can contain one or more paths to output files (search for the `output` key in config files). Be careful not to override files you want to keep, especially when using config files from unknown origins. If not output is specified, `out.png` is used.

Alternatively, the config file can be piped to fordite's `stdin`, if no config files where specified:

```cat <config file> | cargo run --release```

Config files
------------

Config files are json files with instructions on how to generate a fordite image.

No detailed documentation for config files exists yet, because the format is a work in progress. Samples can be found in [`sample_cfg`](sample_cfg), there is also a short readme with explanations of the most important parameters.
